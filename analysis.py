#!/usr/bin/env python3

"""
Food review analysis using nltk
"""


from nltk.sentiment.vader import SentimentIntensityAnalyzer


REVIEW_DATA_FILE = "foods.txt"

ID_START_FROM = len('product/productId: ')
REVIEW_START_FROM = len('review/text: ')


def group_product_by_id():
  """
  Break data into product_id: review_text dictionary list
  """
  all_reviews = []

  with open(REVIEW_DATA_FILE, 'r') as data_file:
    counter = 0

    for line_number, line in enumerate(data_file):
      if line.startswith('product/productId'):
        current_id = line[ID_START_FROM:].strip()
      elif line.startswith('review/text'):
        current_review = line[REVIEW_START_FROM:].strip()
        all_reviews.append((current_id, current_review))

  return(all_reviews)


def analysis_by_product_id(reviews):
  sid = SentimentIntensityAnalyzer()
  results = {}

  for review in reviews:
    product, text = review;
    sentiment = sid.polarity_scores(text)
    if product not in results:
      results[product] = dict(sentiment)
      results[product]['count'] = 1
    else:
      for attr in sentiment:
        results[product][attr] += sentiment[attr]
      results[product]['count'] += 1

  for product in results:
    for attr in results[product]:
      if attr != 'count':
        results[product][attr] /= results[product]['count']
    del results[product]['count']
    del results[product]['compound']

  return results


def analyze_all():
  reviews = group_product_by_id()
  results = analysis_by_product_id(reviews)
  out_file = 'results.txt'
  with open(out_file, 'a') as result_file:
    for result in results:
      result_file.write("{}: {}\n".format(result, results[result]))

if __name__ == '__main__':
  analyze_all()
